package com.shuan.appapirestretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.shuan.appapirestretrofit.interfaces.IJsonPlaceHolderApi;
import com.shuan.appapirestretrofit.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public static final String TAG= MainActivity.class.getSimpleName();
    TextView txtContenidoJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,">>>>Metodo OnCreate");
        txtContenidoJson = findViewById(R.id.txtContenidoJson);

        this.getPost(); //Llamado al Retrofit
    }

    private void getPost(){
        Log.d(TAG,">>>>Metodo MainActivity.getPost()");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IJsonPlaceHolderApi iJsonPlaceHolderApi = retrofit.create(IJsonPlaceHolderApi.class);

        Call<List<Post>> call =  iJsonPlaceHolderApi.getPost();

        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                Log.d(TAG,">>>>Metodo call.enqueue.onResponse");
                if(!response.isSuccessful()){
                    txtContenidoJson.setText("Codigo: "+response.code());
                    return;
                }

                List<Post> lsPost = response.body();

                String contenido = "";
                for (Post post : lsPost){
                    contenido+="userId"+post.getUserId()+"\n";
                    contenido+="id"+post.getId()+"\n";
                    contenido+="title"+post.getTitle()+"\n";
                    contenido+="body"+post.getBody()+"\n";
                    txtContenidoJson.append(contenido);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.d(TAG,">>>>Metodo call.enqueue.onFailure");
                txtContenidoJson.setText("ERROR: "+t.getMessage());
            }
        });
    }
}
