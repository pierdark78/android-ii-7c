package com.shuan.appapirestretrofit.interfaces;

import com.shuan.appapirestretrofit.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IJsonPlaceHolderApi {

    //Metodo encargado de obtener la informacion del API REST
    @GET("posts")
    Call<List<Post>> getPost();
}
