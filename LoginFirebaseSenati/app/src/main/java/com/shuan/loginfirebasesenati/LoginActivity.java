package com.shuan.loginfirebasesenati;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG="LoginActivity";

    public static int SIGN_IN_GOOGLE_CODE = 1;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private SignInButton btnSignInGoogle;
    private GoogleApiClient googleApiClient;

    EditText edtLogin, edtClave;
    Button btnIngresar, btnNuevo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();

        initialize();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickIngresar(edtLogin.getText().toString(),edtClave.getText().toString());   initialize();
                Intent intent = new Intent(getApplicationContext(), BienvenidaActivity.class);
                startActivity(intent);
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickNuevoUsuario(edtLogin.getText().toString(),edtClave.getText().toString());
            }
        });

        btnSignInGoogle.setOnClickListener(new View.OnClickListener() {
         @Override
          public void onClick(View view) {
             Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
             startActivityForResult(intent,SIGN_IN_GOOGLE_CODE);
         }
                                           }
        );
    }

    private void initialize(){
        Log.d(TAG,"Metodo initialize()");

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

                if(firebaseUser!= null){
                    Log.d(TAG,">>>onAuthStateChanged - SignIn()");
                    Log.d(TAG,">>>UserID - "+firebaseUser.getUid());
                    Log.d(TAG,">>>Email - "+firebaseUser.getEmail());
                }else{
                    Log.d(TAG,">>>onAuthStateChanged - SignOut()");
                }
            }
        };

         Log.d(TAG,">>>>Inicio de GoogleSignInOptions");
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
    }

    private void onClickIngresar(String email, String clave){
        firebaseAuth.signInWithEmailAndPassword(email,clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Usuario Logueado", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Fallo el Inicio de Sesión", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void onClickNuevoUsuario(String email, String clave){
        firebaseAuth.createUserWithEmailAndPassword(email,clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Usuario Creado", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Fallo la Creación de Usuario", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void SignInGoogleFirebase(GoogleSignInResult googleSignInResult){
        if(googleSignInResult.isSuccess()){
            AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInResult.getSignInAccount().getIdToken(),null);
            firebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()) {
                        Toast.makeText(LoginActivity.this, "Usuario logueado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),BienvenidaActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(LoginActivity.this, "Fallo el inicio de Sesion", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else {
            Toast.makeText(this, "Fallo el inicio de Sesion", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,">>>>Inicio de Activity Result");
        if(requestCode == SIGN_IN_GOOGLE_CODE){
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            SignInGoogleFirebase(googleSignInResult);
        }
    }

    protected void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    protected void onStop(){
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }

    protected void initObjects(){
        edtLogin = findViewById(R.id.edtLogin);
        edtClave = findViewById(R.id.edtClave);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnNuevo = findViewById(R.id.btnNuevoUsuario);
        btnSignInGoogle = findViewById(R.id.btnSignInGoogle);
    }
}
