package com.shuan.loginfirebasesenati;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class BienvenidaActivity extends AppCompatActivity {
    public static final String TAG="BienvenidaActivity";

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    TextView txtDetalleUsuario;
    Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);
        initObjects();
        initialize();
    }

    protected void initObjects(){
        txtDetalleUsuario = findViewById(R.id.txtDetalleUsuario);
        btnSalir = findViewById(R.id.btnNuevoUsuario);
    }

    private void initialize(){
        Log.d(TAG,"Metodo initialize()");

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

                if(firebaseUser!= null){

                    txtDetalleUsuario.setText("User ID - "+firebaseUser.getUid());
                    Log.d(TAG,">>>onAuthStateChanged - SignIn()");
                    Log.d(TAG,">>>UserID - "+firebaseUser.getUid());
                    Log.d(TAG,">>>Email - "+firebaseUser.getEmail());
                }else{
                    Log.d(TAG,">>>onAuthStateChanged - SignOut()");
                }
            }
        };
    }

    protected void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    protected void onStop(){
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }
}
