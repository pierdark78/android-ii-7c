select * from Curso
delete Curso where idCur > 2;

truncate table Curso;

INSERT INTO CURSO VALUES ('Visual Studio 2019','1','15.25','Lunes a Viernes','','1');
INSERT INTO CURSO VALUES ('Bases de datos','2','36.45','Lunes, Miercoles y Viernes','','2');
INSERT INTO CURSO VALUES ('Fundamentos de Programación','3','50.2','Lunes y Jueves','','1');
INSERT INTO CURSO VALUES ('Desarrollo Móvil','4','14.25','Lunes a Viernes','','3');

update Curso set urlFotoCur='https://static.platzi.com/media/courses/og-prog-b%C3%A1sica-2017.png' where idCur is not null;