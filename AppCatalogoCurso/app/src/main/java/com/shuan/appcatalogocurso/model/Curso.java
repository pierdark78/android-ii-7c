package com.shuan.appcatalogocurso.model;

public class Curso {
    private int idCur;
    private String nombreCur;
    private int creditosCur;
    private Double preciosCur;
    private String horarioCur;
    private String urlFotoCur;
    private int idTipoCur;

    public Curso() {
    }

    public Curso(int idCur, String nombreCur, int creditosCur, Double preciosCur, String horarioCur, String urlFotoCur, int idTipoCur) {
        this.idCur = idCur;
        this.nombreCur = nombreCur;
        this.creditosCur = creditosCur;
        this.preciosCur = preciosCur;
        this.horarioCur = horarioCur;
        this.urlFotoCur = urlFotoCur;
        this.idTipoCur = idTipoCur;
    }

    public int getIdCur() {
        return idCur;
    }

    public void setIdCur(int idCur) {
        this.idCur = idCur;
    }

    public String getNombreCur() {
        return nombreCur;
    }

    public void setNombreCur(String nombreCur) {
        this.nombreCur = nombreCur;
    }

    public int getCreditosCur() {
        return creditosCur;
    }

    public void setCreditosCur(int creditosCur) {
        this.creditosCur = creditosCur;
    }

    public Double getPreciosCur() {
        return preciosCur;
    }

    public void setPreciosCur(Double preciosCur) {
        this.preciosCur = preciosCur;
    }

    public String getHorarioCur() {
        return horarioCur;
    }

    public void setHorarioCur(String horarioCur) {
        this.horarioCur = horarioCur;
    }

    public String getUrlFotoCur() {
        return urlFotoCur;
    }

    public void setUrlFotoCur(String urlFotoCur) {
        this.urlFotoCur = urlFotoCur;
    }

    public int getIdTipoCur() {
        return idTipoCur;
    }

    public void setIdTipoCur(int idTipoCur) {
        this.idTipoCur = idTipoCur;
    }
}
