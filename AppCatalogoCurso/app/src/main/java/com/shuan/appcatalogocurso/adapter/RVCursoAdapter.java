package com.shuan.appcatalogocurso.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.shuan.appcatalogocurso.R;
import com.shuan.appcatalogocurso.model.Curso;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RVCursoAdapter extends RecyclerView.Adapter<RVCursoAdapter.CursoViewHolder> {
    private static final String TAG = RVCursoAdapter.class.getSimpleName();

    private ArrayList<Curso> lsCursos;
    private Activity activity;
    private int resource;

    public RVCursoAdapter(ArrayList<Curso> lsCursos, Activity activity, int resource) {
        this.lsCursos = lsCursos;
        this.activity = activity;
        this.resource = resource;
    }

    @NonNull
    @Override
    public CursoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        CursoViewHolder cursoViewHolder = new CursoViewHolder(view);
        return cursoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CursoViewHolder holder, int position) {
        Curso curso = lsCursos.get(position);
        Picasso.get().load(curso.getUrlFotoCur()).into(holder.imgUrlFotoCard);
        holder.txtNombresCard.setText(curso.getNombreCur());
        holder.txtcreditosCard.setText(curso.getCreditosCur());
        holder.txtPrecioCard.setText(Integer.parseInt(curso.getPreciosCur().toString()));
        holder.txtHorarioCard.setText(curso.getHorarioCur());
        holder.txtIdTipoCurCard.setText(curso.getIdTipoCur());
    }

    @Override
    public int getItemCount() {
        return lsCursos.size();
    }


    //Clase embebida
    public class CursoViewHolder extends RecyclerView.ViewHolder{
        ImageView imgUrlFotoCard;
        TextView txtIdCard, txtNombresCard, txtcreditosCard, txtPrecioCard, txtHorarioCard, txtIdTipoCurCard;
        CardView cvCurso;
        public CursoViewHolder(@NonNull View itemView) {
            super(itemView);

           cvCurso = itemView.findViewById(R.id.cvCurso);
            txtIdCard = itemView.findViewById(R.id.txtIdCard);
            txtNombresCard = itemView.findViewById(R.id.txtNombreCard);
            txtcreditosCard = itemView.findViewById(R.id.txtCreditosCard);
            txtPrecioCard = itemView.findViewById(R.id.txtPrecioCard);
            txtHorarioCard = itemView.findViewById(R.id.txtHorarioCard);
            txtIdTipoCurCard = itemView.findViewById(R.id.txtIdTipoCurCard);
        }
    }
}
