package com.shuan.appcatalogocurso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.shuan.appcatalogocurso.adapter.RVCursoAdapter;
import com.shuan.appcatalogocurso.model.Curso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String TAG=MainActivity.class.getSimpleName();
    CardView cvCurso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initObjects();

        //Manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        //Adapter
        RVCursoAdapter rvCurso = new RVCursoAdapter(listarCursos(), this, R.layout.cardview_curso);
        //Recycler
    }

    private ArrayList<Curso> listarCursos() {
        return null;
    }

    private void initObjects(){
        Log.d(TAG,">>>Metodo initObjects");
        cvCurso = findViewById(R.id.cvCurso);
    }
}
