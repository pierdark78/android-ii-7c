package com.shuan.googlepuntodeinteres;

import androidx.fragment.app.FragmentActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnPoiClickListener {
    public static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnPoiClickListener(this);

        // Add a marker in Sydney and move the camera
        LatLng lima = new LatLng(-12.004987503058528, -76.85210811905422);
        mMap.addMarker(new MarkerOptions().position(lima).title("Marker in Lima"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lima));
    }

    private void showDialog(PointOfInterest poi){
        Log.d(TAG,">>>Metodo ShowDialog");
        AlertDialog.Builder msDialog = new AlertDialog.Builder(this);
        msDialog.setTitle("Punto de Interes");
        msDialog.setMessage("ID:"+poi.placeId+"\nNombre:"+poi.name+"\nLatitud:"+poi.latLng.latitude+"\nLongitud:"+poi.latLng.longitude);
        msDialog.setCancelable(false);

        msDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MapsActivity.this, "Salir", Toast.LENGTH_SHORT).show();
            }
        });

        msDialog.show();
    }

    @Override
    public void onPoiClick(PointOfInterest poi) {
        if (poi!= null) {
            showDialog(poi);
        }
    }
}
