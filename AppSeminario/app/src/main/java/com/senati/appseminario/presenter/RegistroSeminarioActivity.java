package com.senati.appseminario.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.senati.appseminario.R;
import com.senati.appseminario.conexion.DBManager;
import com.senati.appseminario.model.Asistente;

public class RegistroSeminarioActivity extends AppCompatActivity {
    public static  final String TAG= "RegistSeminarioActivity";
    EditText edtIdAsisReg, edtNombresAsisReg, edtApellidosAsisReg, edtFecNacAsisReg, edtDniAsisReg, edtSexoAsisReg, edtEmailAsisReg, edtTelefonoAsisReg;
    Button btnRegistrar, btnSalir;
    DBManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_seminario);
        this.initObjectsReg();

        manager = new DBManager(this );
    }

    public void onClickRegistrar(View view){

        Log.d(TAG,">>> Metodo onClickRegistrar()");

        Asistente asistente = new Asistente();
        //asistente.setIdAsis(edtIdAsisReg.getId());
        asistente.setNombresAsis(edtNombresAsisReg.getText().toString());
        asistente.setApellidosAsis(edtApellidosAsisReg.getText().toString());
        //asistente.setFecNacAsis(edtFecNacAsisReg.);
        asistente.setDniAsis(edtDniAsisReg.getText().toString());
        asistente.setSexoAsis(edtSexoAsisReg.getText().toString());
        asistente.setEmailAsis(edtEmailAsisReg.getText().toString());
        asistente.setTelefonoAsis(edtTelefonoAsisReg.getText().toString());

        long result = manager.insertarAsistente(asistente);

        if( result != -1){
            String mensaje="Registro: "+ result+ " insertado correctamente";
            Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Error de insercion", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickSalir(View view){
        Log.d(TAG,">>> Metodo onClickSalir()");
        Intent intent = new Intent(this, InicioSeminarioActivity.class);
        startActivity(intent);
    }

    protected void initObjectsReg(){
        edtIdAsisReg = findViewById(R.id.edtIdAsiReg);
        edtNombresAsisReg = findViewById(R.id.edtNombresAsisReg);
        edtApellidosAsisReg = findViewById(R.id.edtApellidosAsisReg);
        edtFecNacAsisReg = findViewById(R.id.edtFecNacAsisReg);
        edtDniAsisReg = findViewById(R.id.edtDniAsisReg);
        edtSexoAsisReg = findViewById(R.id.edtSexoAsisReg);
        edtEmailAsisReg = findViewById(R.id.edtEmailAsisReg);
        edtTelefonoAsisReg = findViewById(R.id.edtTelefonoAsisReg);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnSalir = findViewById(R.id.btnSalir);
    }
}
