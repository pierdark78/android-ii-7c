package com.shuan.markergooglemaps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyMarker {
    Context context;

    public MyMarker(Context context) {
        this.context = context;
    }

    public MarkerOptions obtenerMarker(LatLng latLng, String title, int idImg, String description, boolean visible){
        /*
        Posicion del marker en el mapa (latitud, longitud)
        .position(peru)

        Titulo del Marker
        .title("Estoy en Peru")

        El usuario puede mover el marker
        .draggable(true)

        Agrega descripcion breve al Marker
        .snippet("Un lugar muy atractivo")

        Permite rotar el Marker
        .rotation(90f)

        Agrega opacidad al Marker
        .alpha(0.5f)

        Visibilidad del Marker
        .visible(true)
         */

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng)
                .title(title)
                .icon(corrigeSizeIcon(idImg))
                .snippet(description)
                .visible(visible);
        return markerOptions;
    }

    private BitmapDescriptor corrigeSizeIcon(int idImg) {
        int alto=60;
        int ancho=60;
        BitmapDrawable bitmapDrawable = (BitmapDrawable) ContextCompat.getDrawable(context,idImg);
        Bitmap bitmap = bitmapDrawable.getBitmap();

        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap,ancho,alto,false);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);

    }
}
