﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Datos.Models;

namespace ApiRestAndroid.Controllers
{
    public class CursoController : ApiController
    {
        // GET: api/Curso

        [HttpGet]
        public IEnumerable<Curso> Get()
        {
                var lsCursos = new List<Curso>();
            using (db_androidEntities1 db = new db_androidEntities1())
            {
                lsCursos = db.Curso.ToList();
            }
                return lsCursos;
            }

        // GET: api/Curso/5

        [HttpGet]
        public Curso Get(int id)
        {
        var curso = new Curso();
        using (db_androidEntities1 db = new db_androidEntities1())
        {
           curso = db.Curso.FirstOrDefault(x => x.idCur == id);
        }
            return curso;
        }

        // POST: api/Curso

        [HttpPost]
        public void Post([FromBody]string value)
        {
            using (db_androidEntities1 db = new db_androidEntities1())
            {
                var curso = new Curso();
                curso.idCur = 0;
                curso.nombreCur = "Mi nuevo Curso";
                curso.precioCur = 25;
                curso.creditoCur = 5;
                curso.TipoCurso = 1;
                curso.urlFotoCur = "";
                db.SaveChanges();
            }
        }
    }

        // PUT: api/Curso/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Curso/5
        public void Delete(int id)
        {
        }
    }
}
