package com.shuan.appdecorreos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    EditText edtEmail, edtClave;
    Button btnIngresar, btnNuevoRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initObjects();

        this.initialize();

    }

    public void nuevoRegistro(String email, String clave){
        Log.d(TAG,">>>Nuevo Registro()");
        firebaseAuth.createUserWithEmailAndPassword(email,clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Cuenta creada", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Error de creacion", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    protected void initObjects(){
        edtEmail = findViewById(R.id.edtEmail);
        edtClave = findViewById(R.id.edtClave);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnNuevoRegistro = findViewById(R.id.btNuevo);
    }

    public void onClickIngresar(View view){

    }

    public void NuevoRegistro(View view){
        String email = edtEmail.getText().toString();
        String clave = edtClave.getText().toString();
        this.nuevoRegistro(email,clave);
    }

    private void initialize(){
        Log.d(TAG,">>>Metodo initialize()");
        firebaseAuth  =FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if(firebaseAuth!=null) {
                    Log.d(TAG, ">>>Usuario logueado:");
                    Log.d(TAG, ">>>UserID: " + firebaseUser.getUid());
                    Log.d(TAG, "Email: " + firebaseUser.getEmail());
                }else{
                    Log.d(TAG,"Usuario desconectado");
                }
            }
        };
    }
}
